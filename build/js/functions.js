const selects = document.querySelectorAll('.custom-select select');

selects.forEach(select => select.addEventListener('change', function(){
  if(select.value != "") {
    select.parentNode.classList.add('active');
  } else {
    select.parentNode.classList.remove('active');
  }
  console.log(select.value);
}));
